﻿using UnityEngine;
using System.Collections;

public class SpawnController : MonoBehaviour {


	public Transform[] site ;
	public int totalSites;
	public float spawnRate = 5f;
	public int maxAliens = 10;
	public static int alienCount;






	void activateSite(){
		if (alienCount < maxAliens){
			int index=Random.Range (0, totalSites);
			site [index].gameObject.GetComponent<Spawning> ().active = true;
			//print (index);
		}
	}




	// Use this for initialization
	void Start () {
		alienCount = 0;
		InvokeRepeating ("activateSite", spawnRate, spawnRate);

	}
	

}
