﻿using UnityEngine;
using System.Collections;

public class Spaceship : MonoBehaviour {


	public float height= 3f;
	public bool goingRight ;
	public float speed=5f;



	//decrepated

	//checks limits of horizontal movement and decides moving direction
	/*
	void checkCollision() {
				if (transform.position.x < -37f) {
						goingRight = true;
				} else if (transform.position.x > 37f) {
						goingRight = false;
				}
		}
	*/

	//sets velocity acc to moving direction
	void setMotion() {
		if (goingRight)
				rigidbody2D.velocity = new Vector2 (speed, 0);
		else 
				rigidbody2D.velocity = new Vector2 (-speed, 0);
		transform.position = new Vector2 (transform.position.x, height);
	}


	void OnTriggerEnter2D(Collider2D col){
		if(col.tag == "Background")
			goingRight = !goingRight;
	}
	
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
	
		//checkCollision ();
		setMotion ();

	}
}
