﻿using UnityEngine;
using System.Collections;

public class Portal : MonoBehaviour {




	public bool hit;
	public LayerMask hitableObjects;
	public Transform OtherPortal = null;
	public bool teleported = false;
	

/*
	IEnumerator DetectImpact() {
		hit = Physics2D.OverlapCircle (transform.position, 4f, hitableObjects);
		if (hit) {
			print ("HIT HIT HIT");
			rigidbody2D.isKinematic = true;
			yield return new WaitForSeconds(4f);
			Destroy(gameObject);
			
		}
	}
*/
	IEnumerator delay() {
		yield return new WaitForSeconds (1f);
		teleported = false;
		
	}

	void OnTriggerEnter2D(Collider2D col){
		bool gate1;
		bool gate2;

		if(col.tag=="Enemy" || col.tag=="Player"){ 

			
			//entering 1st gate
			if(col.gameObject.GetComponent<PortalController>().gate1Entered==false && col.gameObject.GetComponent<PortalController>().gate2Entered==false){ 
				col.gameObject.GetComponent<PortalController>().gate1Entered=true; 
				col.gameObject.GetComponent<PortalController>().gate2Entered=false;
			}
			//entering 2nd gate
			else if(col.gameObject.GetComponent<PortalController>().gate1Entered==true && col.gameObject.GetComponent<PortalController>().gate2Entered==false){
				col.gameObject.GetComponent<PortalController>().gate1Entered=true; 
				col.gameObject.GetComponent<PortalController>().gate2Entered=true;
			}

			//teleporting
			if (col.gameObject.GetComponent<PortalController>().gate1Entered==true && col.gameObject.GetComponent<PortalController>().gate2Entered==false){
				col.transform.position = OtherPortal.position + new Vector3(2,0,0); ////error is raised here...!!!!!!!
			}

		}
	}


	void OnTriggerExit2D(Collider2D col){
		if (col.tag=="Enemy" || col.tag=="Player"){

			//exiting 2nd gate
			if(col.gameObject.GetComponent<PortalController>().gate1Entered==true && col.gameObject.GetComponent<PortalController>().gate2Entered==true){
				col.gameObject.GetComponent<PortalController>().gate1Entered=false; 
				col.gameObject.GetComponent<PortalController>().gate2Entered=false;
			}

		}
	}



	// Use this for initialization
	void Start () {
		StartCoroutine (delay());	
	}
	
	// Update is called once per frame
	void FixedUpdate () {
	//	DetectImpact ();

		Destroy (gameObject, 10);

	}
}
