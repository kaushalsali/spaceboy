﻿using UnityEngine;
using System.Collections;

public class Health : MonoBehaviour {


	public GUIElement gui;
	public int hp;


	public Health(){
		hp = 10;
	}

	public Health(int hp) {
		this.hp = hp;
	}



	public void reduceHp () {
		hp -= 1;
	}

	void OnGUI(){
		gui.guiText.text = ("HEALTH: " + SpaceBoy.health.hp);
	}

}
