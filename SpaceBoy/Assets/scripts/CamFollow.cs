﻿using UnityEngine;
using System.Collections;

public class CamFollow : MonoBehaviour {





	public Transform player;
	float minX = 13.25f;
	float maxX = -13.25f;
	float minY = 12.7f;
	float maxY = 4.3f;



	void setCamPosition() {
		transform.position = new Vector3 (Mathf.Clamp ( Mathf.Lerp( transform.position.x, player.position.x, Time.deltaTime *1f ), maxX , minX ), transform.position.y, transform.position.z);
		transform.position = new Vector3 (transform.position.x,(Mathf.Clamp ( Mathf.Lerp( transform.position.y, player.position.y, Time.deltaTime *1f ), maxY , minY )), transform.position.z);
		}





	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if( (Mathf.Abs(transform.position.x -player.position.x) > 3f) ||(Mathf.Abs(transform.position.y -player.position.y)>3f) )
			setCamPosition();
	}
}
