﻿using UnityEngine;
using System.Collections;

public class Alien : MonoBehaviour {


	Animator anim;
	public Health health;
	//public Transform player;
	//death explosion variables

	public GameObject explosion;
	public LayerMask bulletLayer;
	GameObject explosionClone;
	public bool hit;
	public SpawnController spawnControl;
	//movement variables
	public bool facingRight = false;
	public LayerMask groundLayer;
	public LayerMask collisionLayer;
	public Transform groundChecker;
	public Transform collisionChecker;
	public bool grounded = false;
	public bool collided = false;
	public float firstJump;
	public float jumpInterval;


	void move(){
		grounded = Physics2D.OverlapCircle (groundChecker.position, 0.2f, groundLayer);
		anim.SetBool ("grounded", grounded);
		if(grounded){
			if (facingRight)
				rigidbody2D.velocity= new Vector2(10f ,rigidbody2D.velocity.y);
			else
				rigidbody2D.velocity=new Vector2(-10f ,rigidbody2D.velocity.y);
		}
	}

	
	void checkFlip() {
		collided = Physics2D.OverlapCircle (collisionChecker.position, 0.2f, collisionLayer);
		Collider2D col = Physics2D.OverlapCircle (collisionChecker.position,0.2f,collisionLayer);
		if (collided && col.isTrigger==false){   //can be wiritten in ontTigger function too.
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale=theScale;
		}
	}


	void jump(){
		rigidbody2D.AddForce (new Vector2 (0f, 6000f));
	}

	
	//checks if hit by bullet
	void detectImpact() {
			hit = Physics2D.OverlapCircle (transform.position, 3.0f, bulletLayer);
		if (hit) {
			health.reduceHp();
			print(" hit " + health.hp );
			hit = false;
			
		}
	}

	
	//keeps track of health and death
	void checkDeath(){
		if(health.hp <= 0){

			explosionClone = Instantiate(explosion,transform.position,transform.rotation) as GameObject;
			SpawnController.alienCount--;
			print ("alien count: " + SpawnController.alienCount);
			Score.AddToScore();
			print ("score: " + Score.getScore());
			Destroy(explosionClone,0.2f);
			Destroy(gameObject);
			
		}
	}



	void OnTriggerEnter2D(Collider2D col){


		if(col.tag == "Player"){
			print ("boy got hit");
			SpaceBoy.health.reduceHp();
			print ("Spaceboy health: " + SpaceBoy.health.hp);
		}








/*
		if(col.tag=="Bullet"){
			Vector3 hitDirection = (transform.position-player.position + Vector3.up);
			//hitDirection = (hitDirection/hitDirection.magnitude);  //unit vector calculation
			rigidbody2D.AddForce((hitDirection*1500f));
			health.reduceHp();
			//rigidbody2D.AddForce(new Vector2(10f,10f));
			print(" hit " + health.hp );
		}


		if(col.tag == "Enemy" || col.tag == "Background"){
			facingRight = !facingRight;
			Vector3 theScale = transform.localScale;
			theScale.x *= -1;
			transform.localScale=theScale;
		}*/	
	}





	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		health = new Health (5);
	//	score = new Score ();
		InvokeRepeating ("jump", firstJump, jumpInterval);

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		move ();
		checkFlip ();
		detectImpact();
		checkDeath ();
	}
}
