using UnityEngine;
using System.Collections;



public class SpaceBoy : MonoBehaviour {

	Animator anim;
	//movement variables
	public float maxSpeed=10f;
	float movingXDirection;
	public bool facingRight = true;
	public bool onGround = false;
	public bool onSpaceship = false;
	public LayerMask whatIsGround;
	public LayerMask spaceshipLayer;
	public Transform groundChecker;
	GameObject whichSpaceship;
	public bool jetpackMode = false;
	public float flyForce;
	public float jumpForce;
	//health variable
	public static Health health;
	bool hit=false;
	public GameObject explosion;
	GameObject explosionClone;



	void whenToFlip() {
		if (movingXDirection > 0 && !facingRight) {
			this.flip ();
		} 
		else if (movingXDirection < 0 && facingRight) {
			this.flip ();
		}	
	}




	void flip() {
		facingRight = !facingRight;
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale=theScale;
		
	}



	void jump(){
		if (onGround) {
			onGround = false;
			rigidbody2D.AddForce (new Vector2 (rigidbody2D.velocity.x, jumpForce));
		}
	}


	void fly(){

		if(Input.GetKey(KeyCode.UpArrow))
			rigidbody2D.AddForce(new Vector2(0f,flyForce));

		else if(Input.GetKey(KeyCode.DownArrow))
			rigidbody2D.AddForce(new Vector2(0f,-flyForce));

		else if(Input.GetKey(KeyCode.LeftArrow)){
			if(facingRight)
				flip ();
			rigidbody2D.AddForce(new Vector2(-flyForce,0f));
		}

		else if(Input.GetKey(KeyCode.RightArrow)){
			if(!facingRight)
				flip ();
			rigidbody2D.AddForce(new Vector2(flyForce,0f));
		}

	}


		
	void CheckDeath() {

		if (health.hp<0){
			float timeOfDeath = Time.time;
			explosionClone = Instantiate(explosion,transform.position,transform.rotation) as GameObject;
			Destroy(explosionClone,3f);
			Destroy(gameObject);
			float newTime= Time.deltaTime;
			//if (newTime- timeOfDeath >= 2f)
				Application.LoadLevel(0);					
		}
	}



	IEnumerator delayExit() {

	if (health.hp<0){
		
		yield return new WaitForSeconds (3f);
		
	 }
	}



	// Use this for initialization
	void Start () {

		StartCoroutine (delayExit ());
		anim = GetComponent<Animator> ();
	//	groundChecker = GameObject.Find("SpaceBoy").transform;
		health = new Health (10);
		//boyScore = new Score ();

	}
	
	// Update is called once per frame
	void FixedUpdate () {

		//jump parameter setting
		onGround = Physics2D.OverlapCircle (groundChecker.position, 0.2f, whatIsGround); // OverlapCircle returns true when a collider is detected within the circle
		anim.SetBool ("grounded", onGround);

		if(!jetpackMode){
			movingXDirection = Input.GetAxis ("Horizontal"); //GetAxis returns +1 for right and -1 for left
			rigidbody2D.velocity = new Vector2 ((movingXDirection * 10f), rigidbody2D.velocity.y); //sets the velocity of the object/sprite
			anim.SetFloat ("speed", Mathf.Abs (movingXDirection));   //sets the speed parameter acc to move thus sets 'Run' state on or off
		}

		//flip character
		whenToFlip();

		//when on Spaceship
		onSpaceship = Physics2D.OverlapCircle (groundChecker.position, 0.2f, spaceshipLayer);
		if (!jetpackMode && onSpaceship) {
			whichSpaceship = (Physics2D.OverlapCircle (groundChecker.position, 0.2f, spaceshipLayer)).gameObject;
			rigidbody2D.velocity = new Vector2(whichSpaceship.rigidbody2D.velocity.x + rigidbody2D.velocity.x, rigidbody2D.velocity.y );
		}	


		CheckDeath ();
	}
	



	void Update()
	{	
		//fly; Written in Update because fly() contains Input.Getkey() etc. 
		if (jetpackMode)
			fly ();

		//when to jump
		if ( Input.GetKey (KeyCode.Space)) 
			jump ();
				



		//jetpack switch
		if(Input.GetKeyDown (KeyCode.C)){
			jetpackMode=!jetpackMode;
			//this part is written here so that it will be checked only on pressing C, thus saving execution time.
			if(jetpackMode){				
				rigidbody2D.gravityScale = 0;
				anim.SetBool("fly",true);
			}
			else {
				rigidbody2D.gravityScale = 1;
				anim.SetBool("fly",false);
			}
		}		
		
	}




}
