﻿using UnityEngine;
using System.Collections;

public class Spawning : MonoBehaviour {

	public GameObject alien;
	GameObject alienClone; 
	public float spawnTime = 4f;            // How long between each spawn
	public float firstSpawnTime=1f;
	Animator anim;
	public bool active=false;

	
	void spawn() {

		anim.SetTrigger ("spawn");
		alienClone = Instantiate (alien, transform.position, transform.rotation) as GameObject;
		SpawnController.alienCount++;
		print ("alien count: "+SpawnController.alienCount);
		active = false;

	}


	void FixedUpdate(){

		if(active)
			spawn();
	
	}

	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		//InvokeRepeating ("Spawn", firstSpawnTime, spawnTime);
	
	}

}
	


