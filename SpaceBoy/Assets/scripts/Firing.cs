using UnityEngine;
using System.Collections;  class Firing : MonoBehaviour {

	public SpaceBoy character;
	//bullet variables
	public Rigidbody2D bullet;
	public static int bulletCount;
	public Rigidbody2D fireClone;
	//portal variables
	public Rigidbody2D portal;
	public bool portalMode = false;
	public int portalCount = 0;
	public bool portalActivated = false;
	public Rigidbody2D portalGate1;
	public Rigidbody2D portalGate2;




	void fire(){
	
		if (portalMode){
			if(portalCount==0){
				portalGate1 = (Instantiate (portal, new Vector2(transform.position.x ,transform.position.y), transform.rotation)) as Rigidbody2D;
				portalCount++;
				print("clone1" + portalCount);
			}
			else if(portalCount==1){
				portalGate2 = (Instantiate (portal, new Vector2(transform.position.x ,transform.position.y), transform.rotation)) as Rigidbody2D;
				portalCount++;
				print("clone2"+ portalCount);
			}
		}

		else {
			
			if (bulletCount<4){				
				fireClone = (Instantiate (bullet, new Vector2(transform.position.x ,transform.position.y), transform.rotation)) as Rigidbody2D;
				bulletCount++;
				if (character.facingRight)
					fireClone.velocity = new Vector2((gameObject.GetComponentInParent<Rigidbody2D>()).velocity.x  +20f, 0f);
				else
					fireClone.velocity = new Vector2(( gameObject.GetComponentInParent<Rigidbody2D>()).velocity.x -20f, 0f);
			}
		}
	}




	void checkFireMode(){
	
		if (portalMode) {
			if (Input.GetKeyDown (KeyCode.F))
					portalMode = false;
		} else {
				if (Input.GetKeyDown (KeyCode.F))
					portalMode = true;
		}

	}








	// Use this for initialization
	void Start () {
		bulletCount = 0;
	}



	void FixedUpdate() {
	
		if (portalMode && portalCount == 2)  //if 2 portals are present
			portalActivated = true;

		if (portalGate1 == null || portalGate2 == null) //if either portal is not present or destroyed
			portalActivated = false;

		if(portalGate1 == null && portalGate2 == null) //when both portals are destroyed
			portalCount=0;

		if (portalActivated){
			portalGate1.GetComponent<Portal>().enabled = true;
			portalGate2.GetComponent<Portal>().enabled = true;
			portalGate1.GetComponent<Portal>().OtherPortal = portalGate2.transform ;
			portalGate2.GetComponent<Portal>().OtherPortal = portalGate1.transform ;
		}





	}

	// Update is called once per frame
	void Update () {
	
		checkFireMode ();

		if (Input.GetKeyDown (KeyCode.LeftControl)|| (Input.GetKeyDown (KeyCode.LeftControl) && Input.GetKey(KeyCode.LeftArrow)) || (Input.GetKeyDown (KeyCode.LeftControl) && Input.GetKey (KeyCode.RightArrow))){
			fire();
		}
	}


}
