﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

	public static int score;
	public GUIElement gui;


	void Start(){
		score = 0;
	}

	public static void AddToScore() {
		score++;
	}

	public static int getScore(){
		return score;
	}



	void OnGUI(){
		gui.guiText.text = ("Score: " + Score.score.ToString ());
	}


}
