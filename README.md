# SpaceBoy

SpaceBoy is a simple 2D game where you have to kill the alien monsters. The gun can fire lazer beams and can also be used for creating temporary teleportation portals. You can fly around with the jetpack too. Check out the controls in the game.

The game was developed using Unity. The 'SpaceBoy' folder is the Unity project folder. I've added 64 bit builds of the game in the repository, for OSX, Linux and Windows. They're in the 'SpaceBoyBuilds' folder. To play the game just run the appropriate build.
